## Slackible

This is a set of ansible playbooks for configuring a Slackware laptop.

To run (consider using a virtualenv):

    pip install -r requirements.txt
    ./runansible
